<?php

// autoload_files.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    '678829c9bead5c61e091a219cc1aa469' => $baseDir . '/content_templates/init.php',
    '3c417106c7943eb210429eeb706634a8' => $baseDir . '/ajax.php',
    '8cf85a788eeeb270a0e7022345277801' => $baseDir . '/custom_posts/product_custom_posts.php',
);
