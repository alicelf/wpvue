<?php
echo "<ol>";
wp_list_comments();
echo "</ol>";
$req         = get_option( 'require_name_email' );
$aria_req    = ( $req ? " aria-required='true'" : '' );
$commenter   = wp_get_current_commenter();
$name_label  = "Name " . ( $req ? "*" : "" );
$email_label = "Email" . ( $req ? "*" : "" );
$arguments   = [
	'id_form'           => 'am-commentform',
	'id_submit'         => 'am-submit',
	'class_submit'      => 'btn btn-primary',
	'title_reply'       => 'Leave a Reply',
	'title_reply_to'    => 'Leave a Reply to %s',
	'cancel_reply_link' => 'Cancel Reply',
	'label_submit'      => 'Add Review',
	'submit_button'     => '<button name="%1$s" type="submit" id="%2$s" class="%3$s">%4$s</button>',

	'comment_field' => '<div class="form-group col-md-12"><textarea  id="comment" name="comment" rows="5" class="form-control"></textarea><label for="comment">Comment</label></div>',

	'fields' => apply_filters( 'comment_form_default_fields', [

		'author' => '<div class="form-group col-md-12"><input value="' . esc_attr( $commenter[ 'comment_author' ] ) . '" class="form-control" name="author" type="text" id="author"><label  for="author">' . $name_label . '</label></div>',

		'email' => '<div class="form-group col-md-12"><input value="' . esc_attr( $commenter[ 'comment_author_email' ] ) . '" class="form-control" name="email" type="email" id="author"><label for="author">' . $email_label . '</label></div>',

	] )

];


$comments_open = comments_open( get_the_ID() );
if ( $comments_open ) {
	echo "<div class='clearfix'>";
	comment_form( $arguments );
	echo "</div>";
}