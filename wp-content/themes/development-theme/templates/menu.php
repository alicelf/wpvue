<?php
global $_am;
$header_class = $_am[ 'sticky-header' ] ? " sticky-header"
	: " not-sticky-header ";

$header_class .= $_am[ 'opt-header-type' ] === "1" ? " header-rows " : " header-columns ";
?>

<header class="header-wrapper <?php echo $header_class ?>">

	<!--Add/remove am-wrap for swicth full/boxed layout-->
	<div class="header-row am-wrap">

		<div class="logo-column">
			<?php echo material_logo() ?>
		</div>
	  <?php
	  if ( has_nav_menu( 'primary' ) ) {
		  wp_nav_menu( [
			  'show_home'       => true,
			  'menu_class'      => 'nav',
			  'container'       => 'nav',
			  'container_class' => 'menu-column navbar',
			  'theme_location'  => 'primary',
			  'walker'          => new AMenu()
		  ] );
	  } else {
		  $menu_id = wp_create_nav_menu( "Default Theme Menu" );
		  wp_nav_menu( [
			  'show_home'       => true,
			  'menu_class'      => 'nav',
			  'container'       => 'nav',
			  'container_class' => 'menu-column navbar',
			  'walker'          => new AMenu()
		  ] );
	  }
	  ?>

	</div>
</header>

<aside class="app-viewport" id="mobile-navigation-aside">
	<div class="hidden-on-desktop">

		<div class="hide-until-dom-loaded">
			<md-sidenav class="md-left md-fixed " ref="sidebar">
		  <?php echo render_mobile_menu() ?>
			</md-sidenav>
		</div>

		<md-whiteframe md-elevation="3">
			<div class="flex-container-nowrap padding-x-10 mobile-nav-inner-handler">
				<div class="flex-col-50">
			<?php echo material_logo() ?>
				</div>
				<div class="flex-col-50 text-right hide-until-dom-loaded">
					<md-button class="md-raised" @click.native="$refs.sidebar.toggle()">
						<md-icon>menu</md-icon>
					</md-button>
				</div>
			</div>
		</md-whiteframe>

	</div>

</aside>