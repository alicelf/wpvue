<?php
// ============= Divider =============
if ( ! function_exists( 'am_divider' ) ) {
	function am_divider() {
		$height = get_sub_field( 'height' );

		if ( $height ) {
			?>
			<div class="clearfix" style="height:<?php echo $height ?>px"></div>
			<?php
		}
	}
}

$the_id = get_the_ID();
if ( have_rows( 'am-flexible-layout', $the_id ) ) {

	while ( have_rows( 'am-flexible-layout' ) ) {
		the_row();

		if ( get_row_layout() === 'am_divider' ) {
			am_divider();
		}

	}
}