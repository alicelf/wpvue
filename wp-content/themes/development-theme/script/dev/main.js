window.Vue = require('vue/dist/vue')
window.eventHub = require('./store/_events')
window.Vuex = require('vuex')
window.VueResource = require('vue-resource')

// Vue MATERIAL
window.VueMaterial = require('vue-material')
Vue.use(VueMaterial)

// Vue.material.registerTheme( 'default', {
// 	primary: {
// 		color: 'deep-orange',
// 		hue: 700
// 	},
// 	accent: {
// 		color: 'orange',
// 		hue  : 900
// 	}
// })

/**
 * ==================== Components ======================
 */
// Popover
// Vue.component('am-popover', require('./components/Popover/Popover.vue'))
// Vue.component('am-popover-trigger', require('./components/Popover/PopoverTrigger.vue'))
// Vue.component('am-popover-content', require('./components/Popover/PopoverContent.vue'))

// Dropdown/Accordion
// Vue.component('am-dropdown', require('./components/Dropdown/Dropdown.vue'))
// Mini Cart
// Vue.component('minicart', require('./components/WooCart/WooCart.vue'))
// User Profile view
// Vue.component('userprofile', require('./components/Profile/Profile.vue'))
// Flashes
Vue.component('flashmessages', require('./components/Flash/Flash.vue'))
// Slider
// Vue.component('am-slider', require('./components/Slider/index.vue'))
/* =========================== Components End ================================ */



let store = require('./store/storage')
store.commit('setUserdata', AMdefaults.currentUser)

// let router = require('./routes')
//
// router.beforeEach((to, from, next) => {
//
// 	let isLoggedIn = CurrentUser.state.userdata
//
// 	if ('requiresAuth' in to.meta) {
// 		if (to.meta.requiresAuth && !isLoggedIn) {
// 			next({name: 'authscreen'})
// 		}
// 		if (to.meta.requiresAuth === false && isLoggedIn) {
// 			next({name: 'badrequest'})
// 		}
// 	}
// 	next()
// })


require('./script')

let amWoo = AMdefaults.wooOptions || null;

new Vue({
	// 'router': router,
	store,
	el: "#am-appwrap",

	data: {
		currency   : amWoo ? amWoo.woo_currency : '',
		appSettings: AMdefaults,
		authInfo   : AMdefaults.themeSettings.auth_info,

		alertok: {
			type       : 'success',
			contentHtml: 'Success',
			text       : 'Ok'
		},

		alertfail: {
			type       : 'fail',
			contentHtml: 'Fail',
			text       : 'Ok'
		},

	},


	computed: {
		// use dynamic in frontend
		currentUserModel: function() {
			return this.$store.state.currentUser.userdata
		}

	},

	created() {

	},

	mounted() {},

	/**
	 * ==================== App Methods ======================
	 */
	methods: {

		openDialog(ref, params) {
			this[params.alert] = params.data
			this.$refs[ref].open();
		},

		closeDialog(ref) {
			this.$refs[ref].close();
		},
		onClose() {
			let vm = this;
			setTimeout(()=> {
				vm.alertok = {
					type       : 'success',
					contentHtml: 'Success',
					text       : 'Ok'
				};
				vm.alertfail = {
					type       : 'fail',
					contentHtml: 'Fail',
					text       : 'Ok'
				}
			}, 800)
		},

		// Right Sidebar
		// toggleRightSidenav() {
		// 	this.$refs.rightSidenav.toggle();
		// },
		// closeRightSidenav() {
		// 	this.$refs.rightSidenav.close();
		// },
		// handleRightSidenavOpen(ref) {
		// 	console.log('Opened: ' + ref);
		// },
		// handleRightSidenavClose(ref) {
		// 	console.log('Closed: ' + ref);
		// }

	}

});

/**
 * ==================== Modules ======================
 * 14.01.2017
 */
// let amThemeSlider = require('./modules/themeslider')
// amThemeSlider.run()
// let amThemeModal = require('./modules/modal')
// amThemeModal.run()