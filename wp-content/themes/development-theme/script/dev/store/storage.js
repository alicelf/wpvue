/**
 * ==================== Central Backend Storage ======================
 */
module.exports = new Vuex.Store({

	modules: {
		currentUser : require('./modules/user'),
		cartState : require('./modules/cart'),
	},

});