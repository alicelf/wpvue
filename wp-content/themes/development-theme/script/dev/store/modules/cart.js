const state = {
	products: []
}

const mutations = {
	setProducts: function(state, data) {
		state.products = data;
	},

	removeFromCart: function(state, data) {
		state.products.splice(state.products.indexOf(data), 1);
	}
}

module.exports = {
	state,
	mutations
}