const state = {
	userdata: null
}

const mutations = {
	setUserdata(state, data) {
		state.userdata = data
	}
}

module.exports = {
	state,
	mutations
}