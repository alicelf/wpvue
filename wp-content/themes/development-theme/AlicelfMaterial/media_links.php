<?php
/**
 * ==================== Frontend Scripts ======================
 * Table of contents (external libs):
 * jQuery
 * vue-material
 * Slick slider
 * Font Awesome
 * Lightbox
 * Custom scrollbar
 * Cropper
 * Scrollmagic
 * Tweenmax (gsap)
 * Google MDL
 * MDL Icons
 * Animate CSS
 */

add_action( 'wp_enqueue_scripts', 'aa_func_20163119123146' );
function aa_func_20163119123146()
{
	$template_path = get_stylesheet_directory_uri();
	$nodemodules   = $template_path . '/node_modules/';
	$production    = WP_DEBUG === false ? '.min' : null;

	// Slick slider | yarn add slick-carousel
	// wp_enqueue_style( 'slick-style', $nodemodules . 'slick-carousel/slick/slick.css' );
	// wp_enqueue_script( 'slick-script', $nodemodules . 'slick-carousel/slick/slick.min.js', [ 'jquery' ], false, true );

	// Font Awesome | yarn add components-font-awesome
	// wp_enqueue_style( 'font-awesome-styles', $nodemodules . 'components-font-awesome/css/font-awesome.min.css' );

	// Lightbox | yarn add lightbox2
	// wp_enqueue_style( 'lightbox-css', $nodemodules . 'lightbox2/dist/css/lightbox.min.css' );
	// wp_enqueue_script( 'lightbox-js', $nodemodules . 'lightbox2/dist/js/lightbox.min.js', [ 'jquery' ], false, true );

	// Custom Scrollbar | yarn add malihu-custom-scrollbar-plugin
	// wp_enqueue_style( 'scroll-style',
	//		$nodemodules . 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css' );
	// wp_enqueue_script( 'scroll-script',
	//		$nodemodules . 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js', ['jquery'], false, true );

	// if ( is_amuserpage() ) {}
	// yarn add cropper
	// wp_enqueue_style( 'cropperstyle', $nodemodules . 'cropper/dist/cropper.min.css' );
	// wp_enqueue_script( 'cropperscript', $nodemodules . 'cropper/dist/cropper.min.js', [ 'jquery' ], false, true );

	/**
	 * ==================== Scrollmagic ======================
	 * yarn add scrollmagic
	 * yarn add gsap
	 */
//	if( $production ) {
//		// TweenMax animation
//		wp_enqueue_script( 'tween-max',
//			$nodemodules."gsap/src/minified/TweenMax.min.js", ['jquery'], false, true );
//		// ScrollToPlugin
//		wp_enqueue_script( 'scrollto-plugin',
//			$nodemodules."gsap/src/minified/plugins/ScrollToPlugin.min.js",
//			['tween-max'], false, true );
//
//		wp_enqueue_script( 'scrollmagic-script',
//			$nodemodules . 'scrollmagic/scrollmagic/minified/ScrollMagic.min.js', ['jquery'], false, true );
//		wp_enqueue_script( 'scrollmagic-animations',
//			$nodemodules . 'scrollmagic/scrollmagic/minified/plugins/animation.gsap.min.js', ['jquery'], false, true );
//
//	} else {
//		// TweenMax animation
//		wp_enqueue_script( 'tween-max',
//			$nodemodules."gsap/src/uncompressed/TweenMax.js", ['jquery'], false, true );
//		// ScrollToPlugin
//		wp_enqueue_script( 'scrollto-plugin',
//			$nodemodules."gsap/src/uncompressed/plugins/ScrollToPlugin.js",
//			['tween-max'], false, true );
//
//		wp_enqueue_script( 'scrollmagic-script',
//			$nodemodules . 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js', ['jquery'], false, true );
//		wp_enqueue_script( 'scrollmagic-animations',
//			$nodemodules . 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js', ['jquery'], false, true );
//		wp_enqueue_script( 'scrollmagic-indicators',
//			$nodemodules . 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js',
//			['jquery'], false, true );
//	}

	// MDL Icons
	wp_enqueue_style( 'google-material-icons',
		'//fonts.googleapis.com/icon?family=Material+Icons' );

	// Styles
	wp_enqueue_style( 'animate-css', $nodemodules . 'animate.css/animate.min.css' );

	// yarn add bootstrap#v4.0.0-alpha.6
//	 wp_enqueue_style( 'bootstrap-style',
//		 $nodemodules . 'bootstrap/dist/css/bootstrap.min.css' );

	wp_enqueue_style( 'template-base-styles', get_bloginfo( 'stylesheet_url' ) );

	// Theme jQuery
	wp_deregister_script( 'jquery' );
	wp_enqueue_script( 'jquery',
		$nodemodules . 'jquery/dist/jquery.min.js', [], false, true );

	// Vue-material and Bootstrap
	wp_enqueue_style( 'vue-material-and-bootstrap',
		get_stylesheet_directory_uri() . '/style_defaults.css' );

	// Application JS
	wp_enqueue_script( 'AMscript',
		$template_path . "/script/prod/build{$production}.js", [
		'jquery',
	], false, true );

	/**
	 * ==================== Ajax Values ======================
	 * 09.12.2016
	 */
	global $_am;
	$values = [
		'auth_info' => [
			'network_purpose'       => $_am[ 'network-purpose' ],
			'registration_info'     => $_am[ 'network-registration' ],
			'registration_strategy' => $_am[ 'network-confirmation-flow' ]
		]
	];

	$routerPrefix = '';
	//	$routerPrefix = 'projects/someproject/';
	$localize_data = [
		'baseurl'         => get_site_url(),
		'themeurl'        => get_template_directory_uri(),
		'themepath'       => get_template_directory(),
		'ajaxurl'         => admin_url( 'admin-ajax.php' ),
		'wpApiUrl'        => wpApiUrl(),
		'networkEndpoint' => get_am_network_endpoint(),
		'themeSettings'   => $values,
		'uploadDir'       => wp_upload_dir()[ 'basedir' ],
		'currentUser'     => am_user( get_current_user_id() ),
		'routerPrefix'    => $routerPrefix,
		'networkSlug'     => am_profile_slug(),
	];
	if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
		$woo_opts = __woo_options();

		$localize_data [ 'wooOptions' ]   = $woo_opts;
		$localize_data [ 'cart_url' ]     = get_permalink( $woo_opts[ 'woocommerce_cart_page_id' ] );
		$localize_data [ 'shop_url' ]     = get_permalink( $woo_opts[ 'woocommerce_shop_page_id' ] );
		$localize_data [ 'checkout_url' ] = get_permalink( $woo_opts[ 'woocommerce_checkout_page_id' ] );
	}

	wp_localize_script( 'AMscript', 'AMdefaults', $localize_data );

}

/**
 * ==================== Admin Scripts ======================
 */
add_action( 'admin_enqueue_scripts', 'aa_func_20163220053219' );
function aa_func_20163220053219()
{
	$template_path = get_stylesheet_directory_uri();
	$cdnjs         = "//cdnjs.cloudflare.com/ajax/libs/";
	// Font Awesome
	wp_enqueue_style( 'font-awesome',
		$cdnjs . 'font-awesome/4.6.3/css/font-awesome.min.css' );
	wp_enqueue_style( 'mdl-icons', "//fonts.googleapis.com/icon?family=Material+Icons" );

	wp_enqueue_style( 'aa-backend-style',
		$template_path . "/style-parts/backend/init.css" );

	wp_enqueue_script( 'admin-jq-script',
		$template_path . "/style-parts/backend/script/admin-script.js",
		[ 'jquery' ], false, true );
}