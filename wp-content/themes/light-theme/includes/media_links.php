<?php
/**
 * ==================== Styles / Scripts ======================
 */
add_action('wp_enqueue_scripts', 'aa_func_20170617070618');
function aa_func_20170617070618()
{
	// yarn add scrollmagic gsap
	// get_stylesheet_directory() path
	$template_path = get_stylesheet_directory_uri();
	$node_modules = $template_path . "/node_modules/";
	$cache_version = 20170010050037;

	// yarn add slick-carousel
//	 wp_enqueue_style( 'slick-style',
//		 $node_modules . 'slick-carousel/slick/slick.css', [], $cache_version );
//	 wp_enqueue_script( 'slick-script',
//		 $node_modules . 'slick-carousel/slick/slick.min.js', [ 'jquery' ], $cache_version, true );

	// yarn add components-font-awesome
//	 wp_enqueue_style( 'font-awesome-styles',
//		 $node_modules . 'components-font-awesome/css/font-awesome.min.css', [], $cache_version );

	// yarn add bootstrap@4.0.0-beta.2
	wp_enqueue_style( 'bootstrap-style',
		$node_modules . 'bootstrap/dist/css/bootstrap.min.css', [], $cache_version );

	// Lightbox | bower install lightbox2 --save
//	 wp_enqueue_style( 'lightbox-css',
//		 $node_modules . 'lightbox2/dist/css/lightbox.min.css', [], $cache_version );
//	 wp_enqueue_script( 'lightbox-js',
//		 $node_modules . 'lightbox2/dist/js/lightbox.min.js', [ 'jquery' ], $cache_version, true );

	// yarn add malihu-custom-scrollbar-plugin
	// wp_enqueue_style( 'scroll-style',
	//		$node_modules . 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css', [], $cache_version );
	// wp_enqueue_script( 'scroll-script',
	//		$node_modules . 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js', ['jquery'], $cache_version, true );

	wp_enqueue_style( 'google-material-icons', '//fonts.googleapis.com/icon?family=Material+Icons' );

	// yarn add animate.css
//	wp_enqueue_style( 'animate-css', $node_modules . 'animate.css/animate.min.css' );
	wp_enqueue_style( 'theme-init-style', get_bloginfo( 'stylesheet_url' ) );
	wp_enqueue_style( 'template-base-styles', $template_path . "/dist/frontend.style.css", [], $cache_version );

	// Application
	wp_enqueue_script( 'AMscript', $template_path . "/dist/frontend.bundle.js", [], $cache_version, true );
	$data = [
		'baseurl'         => get_site_url(),
		'themeurl'        => get_template_directory_uri(),
		'themepath'       => get_template_directory(),
		'ajaxurl'         => admin_url( 'admin-ajax.php' ),
		'uploadDir'       => wp_upload_dir()[ 'basedir' ],
		'currentUser'     => get_current_user_id(),
	];
	wp_localize_script( 'AMscript', 'AMdefaults', $data );

}